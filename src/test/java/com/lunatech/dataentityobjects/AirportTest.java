package com.lunatech.dataentityobjects;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by John on 4/10/2017.
 */
public class AirportTest {
    @Test
    public void setRunways() throws Exception {
        Airport airport = new Airport();
        airport.setRunways(null);
        Assert.assertTrue(airport.getRunways() != null);
    }

    @Test
    public void compareTo() throws Exception {
        Airport airport = new Airport().setName("test1");
        Airport airport2 = new Airport().setName("test2");
        Assert.assertTrue(airport.compareTo(airport2) < 0);
    }
    @Test
    public void equalsTest() throws Exception {
        Airport airport = new Airport();
        Assert.assertFalse(airport.equals(null));
        Assert.assertFalse(airport.equals(new Runway()));
    }

    @Test
    public void hashCodeAndGetIdAsLong() throws Exception {
        Airport airport = new Airport().setId(123);
        Assert.assertTrue(airport.getIdAsLong().equals(Long.valueOf(123)));
        Assert.assertTrue(airport.hashCode() == Long.valueOf(123).hashCode());
    }

}