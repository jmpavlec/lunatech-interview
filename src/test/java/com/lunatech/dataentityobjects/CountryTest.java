package com.lunatech.dataentityobjects;

import org.apache.commons.collections.CollectionUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.Collections;

public class CountryTest {

    @Test
    public void testSetAirports(){
        Country country = new Country();
        country.setAirports(null);
        Assert.assertNotNull(country.getAirports());
    }

    @Test
    public void testPopulateRunwaySurfacesCanRunWithNoAirports(){
        Country country = new Country();
        country.populateRunwaySurfaces();
        Assert.assertTrue(CollectionUtils.isEmpty(country.getRunwaySurfaces()));
    }

    @Test
    public void testPopulateRunwaySurfacesCanRunWithAirportsButNoRunways(){
        Country country = new Country();
        country.setAirports(Collections.singleton(new Airport()));
        country.populateRunwaySurfaces();
        Assert.assertTrue(CollectionUtils.isEmpty(country.getRunwaySurfaces()));
    }
}