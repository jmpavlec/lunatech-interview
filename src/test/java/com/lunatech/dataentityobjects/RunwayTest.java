package com.lunatech.dataentityobjects;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by John on 4/10/2017.
 */
public class RunwayTest {

    @Test
    public void compareTo() throws Exception {
        Runway runway = new Runway().setSurface("Concrete");
        Runway runway2 = new Runway().setSurface("Asphalt");
        Assert.assertTrue(runway.compareTo(runway2)>0);
    }

}