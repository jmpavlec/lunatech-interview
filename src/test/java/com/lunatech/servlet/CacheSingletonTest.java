package com.lunatech.servlet;

import com.lunatech.dataentityobjects.Country;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CacheSingletonTest {


    @Test
    public void getCountriesForStr() throws Exception {
        Assert.assertTrue(CacheSingleton.getCountriesForStr("DE").size() == 2);
        Assert.assertTrue(CacheSingleton.getCountriesForStr("U").size() == 10);
    }

    @Test
    public void getTop10Countries() throws Exception {
        Assert.assertTrue(CacheSingleton.getTop10Countries().size() == 10);
        Assert.assertTrue(CacheSingleton.getTop10Countries().contains(CacheSingleton.getCountryForCode("US")));
    }

    @Test
    public void getBottom10Countries() throws Exception {
        Assert.assertTrue(CacheSingleton.getBottom10Countries().size() == 10);
        Assert.assertTrue(CacheSingleton.getBottom10Countries().contains(CacheSingleton.getCountryForCode("ZZ")));
    }

    @Test
    public void getTop10Le_ident() throws Exception {
        Assert.assertTrue(CacheSingleton.getTop10Le_ident().size() == 10);
    }

    @Test
    public void getAirportsForIsoCountryCode() throws Exception {
        Assert.assertTrue(CacheSingleton.getAirportsForIsoCountryCode("kh").size() == 14);
    }

    @Test
    public void getRunwaysForAirportId() throws Exception {
        Assert.assertTrue(CacheSingleton.getRunwaysForAirportId(6523).size() == 1);
    }

    @Test
    public void getAllCountries() throws Exception {
        Assert.assertTrue(CacheSingleton.getAllCountries().size() == 247);
    }

    @Test
    public void getCountryForCode() throws Exception {
        Assert.assertTrue(CacheSingleton.getCountryForCode("US").getName().equals("United States"));
        Assert.assertTrue(CacheSingleton.getCountryForCode("us").getName().equals("United States"));
        Assert.assertTrue(CacheSingleton.getCountryForCode("DE").getName().equals("Germany"));
        Assert.assertTrue(CacheSingleton.getCountryForCode("DK").getName().equals("Denmark"));
    }

}