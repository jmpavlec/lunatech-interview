package com.lunatech.servlet;

import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

import static org.junit.Assert.*;

/**
 * Created by John on 4/10/2017.
 */
public class QueryServletTest {
    @Test
    public void init() throws Exception {
       new QueryServlet().init();
    }

    @Test
    public void doGetNoQuery() throws Exception {
        QueryServlet servlet = new QueryServlet();
        HttpServletRequest request = new MockHttpServletRequest(){
            @Override
            public String getParameter(String s) {
                return null;
            }
        };
        servlet.doGet(request, new MockHttpServletResponse());
        Assert.assertNull(request.getAttribute(QueryServlet.ATTR_COUNTRIES));
    }

    @Test
    public void doGetWithParam() throws Exception {
        QueryServlet servlet = new QueryServlet();
        HttpServletRequest request = new MockHttpServletRequest(){
            @Override
            public String getParameter(String s) {
                return "US";
            }
        };
        servlet.doGet(request, new MockHttpServletResponse());
        Assert.assertNotNull(request.getAttribute(QueryServlet.ATTR_COUNTRIES));
    }

}