package com.lunatech.servlet;

import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by John on 4/10/2017.
 */
public class ReportServletTest {

    @Test
    public void doGet() throws Exception {
        ReportServlet servlet = new ReportServlet();
        HttpServletRequest request = new MockHttpServletRequest();
        servlet.doGet(request, new MockHttpServletResponse());
        Assert.assertNotNull(request.getAttribute(ReportServlet.ATTR_TOP10));
        Assert.assertNotNull(request.getAttribute(ReportServlet.ATTR_BOTTOM10));
        Assert.assertNotNull(request.getAttribute(ReportServlet.ATTR_COUNTRIES));
        Assert.assertNotNull(request.getAttribute(ReportServlet.ATTR_RUNWAY_BONUS_HELPERS));
    }

}