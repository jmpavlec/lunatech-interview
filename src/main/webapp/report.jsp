<%@page import="com.lunatech.dataentityobjects.Country"%>
<%@page import="com.lunatech.servlet.RunwayBonusHelper"%>
<%@page import="com.lunatech.servlet.ReportServlet"%>
<%@page import="com.lunatech.servlet.QueryServlet"%>
<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%
    Collection<Country> top10 = (Collection<Country>)request.getAttribute(ReportServlet.ATTR_TOP10);
    Collection<Country> bottom10 = (Collection<Country>)request.getAttribute(ReportServlet.ATTR_BOTTOM10);
    Collection<Country> countries = (Collection<Country>)request.getAttribute(ReportServlet.ATTR_COUNTRIES);
    Collection<RunwayBonusHelper> bonusHelpers = (Collection<RunwayBonusHelper>)request.getAttribute(ReportServlet.ATTR_RUNWAY_BONUS_HELPERS);
%>
<html>
<%@include file="./common-header.jspf"%>
<body>
<div class="backtohome">
    <a href="../"><span class="glyphicon glyphicon-chevron-left"></span>Back to home</a>
</div>
<div class="main">
<div class="center">
    <div class="text-center">
        <h1>Reports</h1>
    </div>
    <div class="well">
         <div class="text-center">
             <h2>Top and Bottom 10 reports</h2>
         </div>
        <div class="row top10">
            <div class="col-sm-4">
                <h3>Top 10 Airport Count</h3>
                <ol>
                <%for(Country country : top10){%>
                  <li>
                    <a href="/query?<%=QueryServlet.PARAM_COUNTRY%>=<%=country.getCode()%>"  target="_blank"><%=country%></a>
                    <span class="badge"><%=country.getAirports().size()%>
                  </li>
                <%}%>
                </ol>
            </div>
            <div class="col-sm-4">
                <h3>Bottom 10 Airport Count</h3>
                <ol>
                <%for(Country country : bottom10){%>
                  <li>
                      <a href="/query?<%=QueryServlet.PARAM_COUNTRY%>=<%=country.getCode()%>"  target="_blank"><%=country%></a>
                      <span class="badge"><%=country.getAirports().size()%>
                  </li>
                <%}%>
                </ol>
            </div>
            <div class="col-sm-4">
               <h3>Top 10 Le_ident count for runways</h3>
               <ol>
                  <%for(RunwayBonusHelper helper : bonusHelpers){%>
                    <li><%=helper.getLe_ident()%><span class="badge"><%=helper.getCount()%></li>
                  <%}%>
              </ol>
            </div>
        </div>
    </div>
    <div class="well">
         <div class="text-center">
            <h2>Surface Type for All Countries</h2>
        </div>
        <div class="row">
        <%for(Country country : countries){%>
            <div class="country-runway col-sm-4">
                <div class="country"><%=country%></div>
                <% boolean prependComma = false;%>
                <div>
                <%
                Collection<String> surfaces = country.getRunwaySurfaces();
                if(CollectionUtils.isNotEmpty(surfaces)){
                %>
                    <%for(String surface : country.getRunwaySurfaces()){%>
                        <%if(prependComma){%>, <%}%><%=surface%><%prependComma = true;%>
                    <%}%>
                <%} else { %>
                    No runways specified.
                <%}%>
                </div>
            </div>
        <%}%>
        </div>
    </div>
</div>
</div>
</body>
</html>
