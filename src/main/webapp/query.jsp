<%@page import="com.lunatech.dataentityobjects.Airport"%>
<%@page import="com.lunatech.dataentityobjects.Country"%>
<%@page import="com.lunatech.dataentityobjects.Runway"%>
<%@page import="com.lunatech.servlet.QueryServlet"%>
<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.collections.CollectionUtils"%>
<%
    String countryInputValue = request.getParameter(QueryServlet.PARAM_COUNTRY);
    Collection<Country> countries = (Collection<Country>)request.getAttribute(QueryServlet.ATTR_COUNTRIES);
%>
<html>
<%@include file="./common-header.jspf"%>
<body>
<div class="backtohome">
    <a href="../"><span class="glyphicon glyphicon-chevron-left"></span>Back to home</a>
</div>
<div class="main">
<div class="center">
    <div class="text-center">
        <h1>Query</h1>
        <div>
            <form action="/query">
             <input type="text" class="input-lg" name="<%=QueryServlet.PARAM_COUNTRY%>" placeholder="Country name or code" value="<%=countryInputValue != null ? countryInputValue : ""%>">
             <input class="btn btn-primary btn-lg" type="submit" value="Search">
            </form>
        </div>
    </div>
    <%if(CollectionUtils.isNotEmpty(countries)){%>
    <%for(Country country : countries){
        Collection<Airport> airports = country.getAirports();
    %>
            <div class="well">
                <div class="country" data-toggle="collapse" data-target="#<%=country.getCode()%>">
                    <%=country%><span class="badge"><%=airports.size()%></span>
                </div>
                <div class="row collapse in" id="<%=country.getCode()%>">
                 <%if(CollectionUtils.isNotEmpty(airports)) {%>
                <%for(Airport airport : airports){%>
                    <div class="col-sm-4 airport">
                        <%=airport%>
                        <%if(CollectionUtils.isNotEmpty(airport.getRunways())) {%>
                            <div class="runway">
                            <% boolean prependComma = false;%>
                            <%for(Runway runway : airport.getRunways()){%>
                                <%if(prependComma){%>, <%}%><%=runway.getSurface()%><%prependComma = true;%>
                             <%}%>
                            </div>
                        <%} else { %>
                             <div class="runway none">
                                No runways
                            </div>
                        <%} %>
                    </div>
                <%}%>
                <%} else { %>
                    <div class="col-sm-4 airport">
                        No airports
                    </div>
                <%}%>
                </div>
            </div>
    <%}%>
    <%} else if(countryInputValue != null) {%>
        <div class="text-center none">
            No countries found.
        </div>
    <%}%>
</div>
</div>
</body>
</html>