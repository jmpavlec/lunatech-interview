package com.lunatech.filehandling;

import com.lunatech.dataentityobjects.DataEntityObject;

import java.util.Collection;

/**
 * Interface to represent a datasource. Could be a csv file, database etc. Just needs to know how to loadAll
 * @param <DataObject>
 */
public interface DataSource<DataObject extends DataEntityObject> {

    public Collection<DataObject> loadAll();
}
