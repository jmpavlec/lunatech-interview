package com.lunatech.filehandling;

import com.lunatech.dataentityobjects.DataEntityObject;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.MappingStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Collection;


/**
 * DataSource that helps load a  CSV file into a collection of {@link DataType}
 * @param <DataType>
 */
public class CSVFileDataSource<DataType extends DataEntityObject> implements DataSource<DataType> {

    private final CSVReader reader;
    private final MappingStrategy<DataType> mappingStrategy;
    private static final Logger LOGGER = LogManager.getLogger("CSVFileDataSource");

    public CSVFileDataSource(String filePath, MappingStrategy<DataType> mappingStrategy) {
        try {
            this.reader = new CSVReader( new InputStreamReader(new FileInputStream(filePath), "UTF-8")); //UTF-8 as the csv file has some international characters
            this.mappingStrategy = mappingStrategy;
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            LOGGER.error(e);
            throw new RuntimeException(e);
        }
    }

    public Collection<DataType> loadAll() {
        CsvToBean<DataType> csvToBean = new CsvToBean<>();
        return csvToBean.parse(mappingStrategy, reader);
    }
}
