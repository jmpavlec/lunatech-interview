package com.lunatech.dataentityobjects;

import com.opencsv.bean.CsvBindByName;

public  abstract class AbstractDataEntityObject<SelfType> implements DataEntityObject {

    @CsvBindByName
    private long id;
    @Override
    public final long getId() {
        return id;
    }
    public SelfType setId(long id) {
        this.id = id;
        return getThis();
    }

    @Override
    public final Long getIdAsLong() {
        return Long.valueOf(getId());
    }

    @Override
    public final int hashCode() {
        return getIdAsLong().hashCode();
    }

    @Override
    public final boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (o.getClass() != this.getClass()) {
            return false;
        }
        DataEntityObject obj = (DataEntityObject)o;
        return getId() == obj.getId();
    }

    public abstract SelfType getThis();
}