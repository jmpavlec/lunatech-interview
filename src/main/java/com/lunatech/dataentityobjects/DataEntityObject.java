package com.lunatech.dataentityobjects;

/**
 * Created by John on 4/8/2017.
 */
public interface DataEntityObject<SelfType> {
    public long getId();
    public Long getIdAsLong();
}
