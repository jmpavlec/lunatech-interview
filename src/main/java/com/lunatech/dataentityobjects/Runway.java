package com.lunatech.dataentityobjects;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import org.apache.commons.lang3.StringUtils;

/**
 * CSV header for Runway
 * "id","airport_ref","airport_ident","length_ft","width_ft","surface","lighted","closed","le_ident","le_latitude_deg",
 * "le_longitude_deg","le_elevation_ft","le_heading_degT","le_displaced_threshold_ft","he_ident","he_latitude_deg",
 * "he_longitude_deg","he_elevation_ft","he_heading_degT","he_displaced_threshold_ft"
 */
public class Runway extends AbstractDataEntityObject<Runway> implements Comparable<Runway> {
    @CsvBindByName
    private long airport_ref;
    @CsvBindByName
    private String airport_ident;
    @CsvBindByName
    private String surface;
    @CsvBindByName
    private String le_ident;


    public long getAirport_ref() {
        return airport_ref;
    }

    public Long getAirport_refAsLong() {
        return Long.valueOf(getAirport_ref());
    }

    public String getAirport_ident() {
        return airport_ident;
    }

    public String getSurface() {
        return StringUtils.isNotEmpty(surface) ? StringUtils.trim(surface) : "None specified";
    }

    public Runway setSurface(String surface) {
        this.surface = surface;
        return this;
    }

    public String getLe_ident() {
        return le_ident;
    }

    public static final MappingStrategy<Runway> getMappingStrategy(){
        HeaderColumnNameMappingStrategy<Runway> strat =  new HeaderColumnNameMappingStrategy<Runway>();
        strat.setType(Runway.class);
        return strat;
    }

    @Override
    public int compareTo(Runway runway) {
        //nullsafe compare
        int compare = StringUtils.compare(getSurface(), runway.getSurface());
        return compare == 0 ? getIdAsLong().compareTo(runway.getIdAsLong()) : compare;
    }

    @Override
    public String toString() {
        return getSurface();
    }

    @Override
    public Runway getThis() {
        return this;
    }
}