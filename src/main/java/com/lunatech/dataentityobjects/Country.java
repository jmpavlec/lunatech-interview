package com.lunatech.dataentityobjects;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import org.apache.commons.lang3.StringUtils;
import com.lunatech.servlet.CacheSingleton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeSet;

/**
 *  CSV header for Country
 *  "id","code","name","continent","wikipedia_link","keywords"
 */
public class Country extends AbstractDataEntityObject<Country> implements Comparable<Country> {
    @CsvBindByName
    private String code;
    @CsvBindByName
    private String name;
    @CsvBindByName
    private String continent;
    @CsvBindByName
    private String wikipedia_link;
    @CsvBindByName
    private String keywords;
    private Collection<Airport> airports = new ArrayList<>();
    private Collection<String> runwaySurfaces = new TreeSet<>();


    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getContinent() {
        return continent;
    }

    public String getWikipedia_link() {
        return wikipedia_link;
    }

    public String getKeywords() {
        return keywords;
    }

    public Collection<Airport> getAirports() {
        return airports;
    }

    public Country setAirports(Collection<Airport> airports) {
        if(airports != null){
            this.airports = airports;
        }
        return this;
    }

    public Country populateRunwaySurfaces(){
        if(airports != null) {
            for (Airport airport : getAirports()) {
                for (Runway runway : airport.getRunways()) {
                    this.runwaySurfaces.add(runway.getSurface());
                }
            }
        }
        return this;
    }

    public Collection<String> getRunwaySurfaces(){
        return runwaySurfaces;
    }

    public static final MappingStrategy<Country> getMappingStrategy(){
        HeaderColumnNameMappingStrategy<Country> strat =  new HeaderColumnNameMappingStrategy<Country>();
        strat.setType(Country.class);
        return strat;
    }

    @Override
    public int compareTo(Country country) {
        return StringUtils.compare(getName(), country.getName());
    }

    @Override
    public String toString() {
        return new StringBuilder(getName()).append(" (").append(getCode()).append(")").toString();
    }

    @Override
    public Country getThis() {
        return this;
    }
}