package com.lunatech.dataentityobjects;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import com.opencsv.bean.MappingStrategy;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;

/**
 * CSV Header for Airport
 * "id","ident","type","name","latitude_deg","longitude_deg","elevation_ft","continent","iso_country","iso_region",
 * "municipality","scheduled_service","gps_code","iata_code","local_code","home_link","wikipedia_link","keywords"
 */
public class Airport extends AbstractDataEntityObject<Airport> implements Comparable<Airport> {
    @CsvBindByName
    private String name;
    @CsvBindByName
    private String ident;
    @CsvBindByName
    private String type;
    @CsvBindByName
    private String iso_country;
    private Collection<Runway> runways = new ArrayList<>();

    public String getName() {
        return name;
    }

    public Airport setName(String name) {
        this.name = name;
        return this;
    }

    public String getIdent() {
        return ident;
    }

    public String getType() {
        return type;
    }

    public String getIso_country() {
        return iso_country;
    }

    public Collection<Runway> getRunways() {
        return runways;
    }

    public Airport setRunways(Collection<Runway> runways) {
        if(runways != null){
            this.runways = runways;
        }
        return this;
    }

    public static final MappingStrategy<Airport> getMappingStrategy(){
        HeaderColumnNameMappingStrategy<Airport> strat =  new HeaderColumnNameMappingStrategy<Airport>();
        strat.setType(Airport.class);
        return strat;
    }

    @Override
    public int compareTo(Airport airport) {
       int compare = StringUtils.compare(getName(), airport.getName());
       return compare == 0 ? getIdAsLong().compareTo(airport.getIdAsLong()) : compare;
    }

    public Airport getThis(){
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder(getName()).append(" (id: ").append(getId()).append(")").toString();
    }
}