package com.lunatech.servlet;

import com.lunatech.dataentityobjects.Country;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet(
        name = "QueryServlet",
        urlPatterns = {"/query"}
)
public class QueryServlet extends HttpServlet {

    public static final String PARAM_COUNTRY = "country";
    public static final String ATTR_COUNTRIES = "countries";
    private static final Logger LOGGER = LogManager.getLogger("QueryServlet");

    @Override
    public void init() throws ServletException {
        super.init();
        CacheSingleton.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.trace("Query doGet start");
        String countryQueryString = request.getParameter(PARAM_COUNTRY);
        if(StringUtils.isNotEmpty(countryQueryString)) {
            LOGGER.info("Querying: "+countryQueryString);
            Collection<Country> countries = CacheSingleton.getCountriesForStr(countryQueryString.trim());
            request.setAttribute(ATTR_COUNTRIES, countries);
        }
        request.setCharacterEncoding("UTF-8");
        RequestDispatcher view = request.getRequestDispatcher("query.jsp");
        view.forward(request,response);
        LOGGER.trace("Query doGet end");
    }
}
