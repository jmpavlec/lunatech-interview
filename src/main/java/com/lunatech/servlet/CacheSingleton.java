package com.lunatech.servlet;

import com.lunatech.dataentityobjects.Airport;
import com.lunatech.dataentityobjects.Country;
import com.lunatech.dataentityobjects.Runway;
import com.lunatech.filehandling.CSVFileDataSource;
import com.lunatech.filehandling.DataSource;
import com.lunatech.launch.Main;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class to store all the data we will need to display for the webapp. It will load the csv files
 * and dump them into some maps that will allow us to 'query' for data.
 */
public class CacheSingleton {
    private static final Logger LOGGER = LogManager.getLogger("CacheSingleton");
    private static final NavigableMap<String, Country> mapCountryByCode = new TreeMap<>();
    private static final NavigableMap<String, Country> mapCountryByName = new TreeMap<>();
    private static final Comparator<Country> AIRPORT_COUNT_COMPARATOR = new Comparator<Country>(){
        //Countries with most airports first
        @Override
        public int compare(Country c1, Country c2) {return c2.getAirports().size() - c1.getAirports().size();}
    };
    private static final List<Country> top10Countries = new ArrayList<>();
    private static final List<Country> bottom10Countries = new ArrayList<>();
    private static final Map<String, Collection<Airport>> mapAirportsByIsoCountryCode = new HashMap<>();
    private static final List<RunwayBonusHelper> runwayLe_identWithCount = new ArrayList<>();
    private static final Map<Long, Collection<Runway>> mapRunwaysByAirportId = new HashMap<>();
    private static final CacheSingleton SINGLETON = new CacheSingleton(Main.getRootPath()).populateCaches();

    private static final String RUNWAYSCSV = "runways.csv";
    private static final String AIRPORTSCSV = "airports.csv";
    private static final String COUNTRIESCSV = "countries.csv";
    private final String resourcePath;
    private CacheSingleton(String rootPath){
        this.resourcePath = rootPath+"/resources/";
    }

    public static final CacheSingleton getInstance(){
        return SINGLETON;
    }

    private DataSource<Runway> getRunwayDataSource(){
        return new CSVFileDataSource<>(resourcePath + RUNWAYSCSV, Runway.getMappingStrategy());
    }
    private DataSource<Airport> getAirportDataSource(){
        return new CSVFileDataSource<>(resourcePath + AIRPORTSCSV, Airport.getMappingStrategy());
    }
    private DataSource<Country> getCountryDataSource(){
        return new CSVFileDataSource<>(resourcePath + COUNTRIESCSV, Country.getMappingStrategy());
    }

    public final CacheSingleton populateCaches(){
        LOGGER.info("Beginning loading of caches");
        populateRunwayCaches();
        populateAirportCaches();
        populateCountryCaches();
        LOGGER.info("Finished loading caches");
        return this;
    }



    private void populateRunwayCaches(){
        LOGGER.info("Loading Runway data");
        Collection<String> runwayLe_ident = new ArrayList<>();
        for(Runway runway : getRunwayDataSource().loadAll()){
            Collection<Runway> runways = mapRunwaysByAirportId.get(runway.getAirport_refAsLong());
            if(CollectionUtils.isEmpty(runways)){
                runways = new TreeSet<>();
            }
            if(StringUtils.isNotEmpty(runway.getLe_ident())){
                runwayLe_ident.add(runway.getLe_ident());
            }
            runways.add(runway);
            mapRunwaysByAirportId.put(runway.getAirport_refAsLong(), runways);
        }
        Map<String,Long> runwayLe_ident_count = runwayLe_ident.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        for(Map.Entry<String, Long> entrySet : runwayLe_ident_count.entrySet()){
            runwayLe_identWithCount.add(new RunwayBonusHelper(entrySet.getKey(), entrySet.getValue().intValue()));
        }
        Collections.sort(runwayLe_identWithCount);
        LOGGER.info("Done loading Runway data");
    }

    private void populateAirportCaches(){
        LOGGER.info("Loading Airport data");
        for(Airport airport : getAirportDataSource().loadAll()){
            Collection<Airport> airports = mapAirportsByIsoCountryCode.get(airport.getIso_country().toUpperCase());
            if(CollectionUtils.isEmpty(airports)){
                airports = new TreeSet<>();
            }
            airports.add(airport.setRunways(mapRunwaysByAirportId.get(airport.getId())));
            mapAirportsByIsoCountryCode.put(airport.getIso_country().toUpperCase(), airports);
        }
        LOGGER.info("Done loading Airport data");
    }

    private void populateCountryCaches(){
        LOGGER.info("Loading Country data");
        List<Country> countriesSortedByAirportCount = new ArrayList<>();
        for(Country country : getCountryDataSource().loadAll()){
            country.setAirports(getAirportsForIsoCountryCode(country.getCode())).populateRunwaySurfaces();
            mapCountryByCode.put(country.getCode().toUpperCase(), country);
            mapCountryByName.put(country.getName().toUpperCase(), country);
            countriesSortedByAirportCount.add(country);
        }
        Collections.sort(countriesSortedByAirportCount, AIRPORT_COUNT_COMPARATOR);
        top10Countries.addAll(countriesSortedByAirportCount.subList(0, 10));
        int size = countriesSortedByAirportCount.size();
        bottom10Countries.addAll(countriesSortedByAirportCount.subList(size-10, size));
        Collections.reverse(bottom10Countries);
        LOGGER.info("Done loading Country data");
    }

    public static Collection<Country> getCountriesForStr(String country) {
        Collection<Country> queriedCountries = new TreeSet<>();
        queriedCountries.addAll(getByStartsWithKey(mapCountryByName, country.toUpperCase()).values());
        queriedCountries.addAll(getByStartsWithKey(mapCountryByCode, country.toUpperCase()).values());
        return queriedCountries;
    }

    public static final Collection<Country> getTop10Countries(){
        return top10Countries;
    }

    public static final Collection<Country> getBottom10Countries() {
        return bottom10Countries;
    }

    public static final Collection<RunwayBonusHelper> getTop10Le_ident(){
        return runwayLe_identWithCount.subList(0, 10);
    }

    public static Collection<Airport> getAirportsForIsoCountryCode(String isoCountryCode){
        return mapAirportsByIsoCountryCode.get(isoCountryCode.toUpperCase());
    }

    public static final Collection<Runway> getRunwaysForAirportId(long id) {
        return mapRunwaysByAirportId.get(Long.valueOf(id));
    }

    public static final Collection<Country> getAllCountries() {
       return mapCountryByCode.values();
    }

    public static final Country getCountryForCode(String countryCode) {
        return mapCountryByCode.get(countryCode.toUpperCase());
    }

    private static final <DataType> SortedMap<String, DataType> getByStartsWithKey(
            NavigableMap<String, DataType> myMap,
            String prefix ) {
        return myMap.subMap( prefix, prefix + Character.MAX_VALUE );
    }
}