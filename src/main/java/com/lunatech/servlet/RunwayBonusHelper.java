package com.lunatech.servlet;

/**
 * Class to help get the answer to the bonus question about most popular le_ident of a runway
 */
public class RunwayBonusHelper implements Comparable<RunwayBonusHelper> {
    private String le_ident;
    private int count;

    public RunwayBonusHelper(String le_ident, int count){
        this.le_ident = le_ident;
        this.count = count;
    }

    public String getLe_ident() {
        return le_ident;
    }

    public int getCount() {
        return count;
    }

    @Override
    public int compareTo(RunwayBonusHelper helper) {
        return helper.count - count;
    }
}
