package com.lunatech.servlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "ReportServlet",
        urlPatterns = {"/report"}
)
public class ReportServlet extends HttpServlet {

    public static final String ATTR_TOP10 = "top10";
    public static final String ATTR_BOTTOM10 = "bottom10";
    public static final String ATTR_COUNTRIES = "countries";
    public static final String ATTR_RUNWAY_BONUS_HELPERS = "runwayBonusHelpers";

    private static final Logger LOGGER = LogManager.getLogger("ReportServlet");

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOGGER.trace("Report doGet start");
        request.setAttribute(ATTR_TOP10, CacheSingleton.getTop10Countries());
        request.setAttribute(ATTR_BOTTOM10, CacheSingleton.getBottom10Countries());
        request.setAttribute(ATTR_COUNTRIES, CacheSingleton.getAllCountries());
        request.setAttribute(ATTR_RUNWAY_BONUS_HELPERS, CacheSingleton.getTop10Le_ident());
        RequestDispatcher view = request.getRequestDispatcher("report.jsp");
        view.forward(request,response);
        LOGGER.trace("Report doGet respond");
    }
}
