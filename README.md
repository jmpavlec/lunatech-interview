# Lunatech Interview Tech Assessment

A basic webapp that reads from some csv files about countries/airports (data from from http://ourairports.com/data/ (released under public domain))
 and allows for querying by country/country code (with partial). It also has a reports page giving you the top/bottom countries by number of airports
 and a summary by country of the runway surfaces.

## Prerequisites installed

Java 8 SDK
Intellij
Maven

## Building and running - command line

Building
```
//with tests
mvn package
//without
mvn package -DskipTests
```

Running
```
//windows
target\bin\webapp.bat
//linux based OS
sh target/bin/webapp
```

You can also follow the steps outlined here to deploy to heroku
https://devcenter.heroku.com/articles/create-a-java-web-application-using-embedded-tomcat#run-your-application

## Tests
Test were written in jUnit4 and can be run via the commandline or within Intellij

## Built With

* [Apache Commons](https://commons.apache.org/) - String and Collection utilities
* [Log4j](https://logging.apache.org/log4j/2.x/) - Logging
* [Maven](https://maven.apache.org/) - Dependency Management
* [OpenCSV](http://opencsv.sourceforge.net/) - CSV parsing framework
* [Tomcat](http://tomcat.apache.org/) - webserver

## Authors

* **John Pavlecich** - [jmpavlec](https://bitbucket.org/jmpavlec/lunatech-interview/src)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details